#include "mbedSpiMasterDriver.h"

MbedSpiMasterDriver::MbedSpiMasterDriver()
{
}
MbedSpiMasterDriver::~MbedSpiMasterDriver()
{
}

void MbedSpiMasterDriver::initCsPin(uint32_t cs)
{
    _SPI_CSEL[cs % 225] = new DigitalOut((PinName)cs);
    _SPI_CSEL[cs % 225]->write(1);
}

void MbedSpiMasterDriver::setPin(uint32_t mosi, uint32_t miso, uint32_t sclk)
{
    _SPI_MOSI = (PinName)mosi;
    _SPI_MISO = (PinName)miso;
    _SPI_SCLK = (PinName)sclk;
}

void MbedSpiMasterDriver::setFrequency(uint32_t freq = 1'000'000)
{
    _SPI_FREQ = freq;
}

void MbedSpiMasterDriver::setMode(uint8_t mode = 0)
{
    _SPI_MODE = mode;
}

int32_t MbedSpiMasterDriver::init()
{
    _pSPI = new SPI(_SPI_MOSI, _SPI_MISO, _SPI_SCLK);
    _pSPI->frequency(_SPI_FREQ);
    _pSPI->format(8, _SPI_MODE);
    return 0;
}

void MbedSpiMasterDriver::deinit()
{
    //NOP, mbed should already deal with this automatically
}

int32_t MbedSpiMasterDriver::write(uint8_t *txbuff, int32_t txlen)
{
    _pSPI->write((const char *)txbuff, txlen, 0, 0);
    return 0;
}

int32_t MbedSpiMasterDriver::read(uint8_t *rxbuff, int32_t rxlen)
{
    _pSPI->write(0, 0, (char *)rxbuff, rxlen);
    return 0;
}

int32_t MbedSpiMasterDriver::writeread(uint8_t *txbuff, int32_t txlen, uint8_t *rxbuff, int32_t rxlen)
{
    _pSPI->write((const char *)txbuff, txlen, (char *)rxbuff, rxlen);
    return 0;
}

void MbedSpiMasterDriver::activateDevice(uint32_t cs)
{
    _SPI_CSEL[cs % 225]->write(0);
}

void MbedSpiMasterDriver::deactivateDevice(uint32_t cs)
{
    _SPI_CSEL[cs % 225]->write(1);
}

// void MbedSpiMasterDriver::reset()
// {
//     //NOP
// }
