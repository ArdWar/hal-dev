#include "mbedLock.h"

satunol::MbedLock::MbedLock()
{
    _pMutex = new Mutex();
}

satunol::MbedLock::~MbedLock()
{
    delete _pMutex;
}

void satunol::MbedLock::lock()
{
    _pMutex->lock();
}

void satunol::MbedLock::unlock()
{
    _pMutex->unlock();
}