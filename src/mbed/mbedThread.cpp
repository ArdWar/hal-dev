#include "mbedThread.h"

satunol::MbedThread::MbedThread()
{
    _pThread = new ::Thread();
}

satunol::MbedThread::~MbedThread()
{
    delete _pThread;
}

void satunol::MbedThread::setPriority(uint8_t priority)
{
    osPriority_t mbedPriority = osPriorityNormal;

    switch (priority)
    {
    case 0:
        mbedPriority = osPriorityIdle;
        break;
    case 1:
        mbedPriority = osPriorityLow;
        break;
    case 2:
        mbedPriority = osPriorityBelowNormal;
        break;
    case 3:
        mbedPriority = osPriorityNormal;
        break;
    case 4:
        mbedPriority = osPriorityAboveNormal;
        break;
    case 5:
        mbedPriority = osPriorityRealtime;
        break;
    }

    _pThread->set_priority(mbedPriority);
}

void satunol::MbedThread::setStackSize(uint32_t size)
{
    // can't set stack size once thread is initialized
}

void satunol::MbedThread::start()
{
    _pThread->start(_pMbedCallback);
}

void satunol::MbedThread::stop()
{
    _pThread->terminate();
}

satunol::Thread *satunol::MbedThread::getCurrentThread()
{
    return this;
}

void satunol::MbedThread::sleep(uint32_t msPeriod)
{
    wait_us(1000* msPeriod);
}

void satunol::MbedThread::setHandler(ThreadHandler *pHandlerObj)
{
    _pMbedCallback = callback(pHandlerObj, &ThreadHandler::run);
}

void satunol::MbedThread::setHandler(void(*pHandlerFunc)())
{
    _pMbedCallback = pHandlerFunc;
}