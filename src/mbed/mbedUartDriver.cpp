
#include "mbedUartDriver.h"

MbedUartDriver::MbedUartDriver(){};
MbedUartDriver::~MbedUartDriver(){};

uint32_t MbedUartDriver::setPin(uint32_t txd, uint32_t rxd)
{
    _UART_TXD = (PinName)txd;
    _UART_RXD = (PinName)rxd;
    return 0;
};

uint32_t MbedUartDriver::setConfig(uint32_t baudRate, uint8_t frameSize, uint8_t parityBit, uint8_t stopBit)
{
    _UART_BAUD = baudRate;
    _UART_BITS = frameSize;
    switch (parityBit)
    {
    case 0:
        _UART_PARITY = BufferedSerial::None;
        break;
    case 1:
        _UART_PARITY = BufferedSerial::Odd;
        break;
    case 2:
        _UART_PARITY = BufferedSerial::Even;
        break;
    }
    _UART_STOP_BITS = stopBit;
    return 0;
};

uint32_t MbedUartDriver::setSwFlowControl()
{
    return 0;
};
uint32_t MbedUartDriver::setHwFlowControl(uint32_t rts, uint32_t cts)
{
    if (rts != 0xff)
    {
        if (cts != 0xff)
        {
            _UART_HWFLOWCTRL = BufferedSerial::RTSCTS;
            _UART_FLOW1 = (PinName)rts;
            _UART_FLOW2 = (PinName)cts;
        }
        else
        {
            _UART_HWFLOWCTRL = BufferedSerial::RTS;
            _UART_FLOW1 = (PinName)rts;
            _UART_FLOW2 = NC;
        }
    }
    else
    {
        if (cts != 0xff)
        {
            _UART_HWFLOWCTRL = BufferedSerial::CTS;
            _UART_FLOW1 = (PinName)cts;
            _UART_FLOW2 = NC;
        }
        else
        {
            _UART_HWFLOWCTRL = BufferedSerial::Disabled;
            _UART_FLOW1 = NC;
            _UART_FLOW2 = NC;
        }
    }
    return 0;
};

uint32_t MbedUartDriver::init()
{
    serial = new BufferedSerial(_UART_TXD, _UART_RXD, _UART_BAUD);
    serial->set_format(_UART_BITS, _UART_PARITY, _UART_STOP_BITS);
    serial->set_flow_control(_UART_HWFLOWCTRL, _UART_FLOW1, _UART_FLOW2);
    return 0;
};

uint32_t MbedUartDriver::deinit()
{
    delete serial;
    return 0;
};

uint32_t MbedUartDriver::putChar(char c)
{
    serial->write(&c, 1);
    return 0;
};
uint32_t MbedUartDriver::putLine(const char *str)
{
    serial->write(str, strlen(str));
    return 0;
};
char MbedUartDriver::getChar()
{
    char c;
    serial->read(&c, 1);
    return c;
};
uint32_t MbedUartDriver::getLine(char *str, uint32_t len)
{
    serial->read(str, len);
    return 0;
};
uint32_t MbedUartDriver::dataAvailable()
{
    return 0;
};
uint32_t MbedUartDriver::powerUp(uint8_t state)
{
    return 0;
};
