#include "mbedI2CMasterDriver.h"

MbedI2CMasterDriver::MbedI2CMasterDriver(){};
MbedI2CMasterDriver::~MbedI2CMasterDriver(){};

void MbedI2CMasterDriver::setPin(uint32_t sda, uint32_t scl)
{
    _I2C_SDA = (PinName)sda;
    _I2C_SCL = (PinName)scl;
}

void MbedI2CMasterDriver::setFrequency(uint32_t clk)
{
    _I2C_CLK = clk;
}

void MbedI2CMasterDriver::active(bool isActive)
{
    // NOP, mbed *should* already take care of peripheral power management automatically
}

int32_t MbedI2CMasterDriver::init()
{
    _pI2C = new I2C(_I2C_SDA, _I2C_SCL);
    _pI2C->frequency(_I2C_CLK);
    return 0;
}

void MbedI2CMasterDriver::deinit()
{
    delete _pI2C;
}

int32_t MbedI2CMasterDriver::read(uint8_t addr7, uint8_t *rxData, int32_t rxLen)
{
    return _pI2C->read(addr7 << 1, (char *)rxData, rxLen) == 0 ? 0 : -1;
}

int32_t MbedI2CMasterDriver::write(uint8_t addr7, uint8_t *txData, int32_t txLen)
{
    return _pI2C->write(addr7 << 1, (char *)txData, txLen) == 0 ? 0 : -1;
}

int32_t MbedI2CMasterDriver::writeread(uint8_t addr7, uint8_t *txData, int32_t txLen, uint8_t *rxData, int32_t rxLen)
{
    if (_pI2C->write(addr7 << 1, (char *)txData, txLen, true))
    {
        return -1;
    }
    return _pI2C->read(addr7 << 1, (char *)rxData, rxLen, false);
}

int32_t MbedI2CMasterDriver::readwrite(uint8_t addr7, uint8_t *rxData, int32_t rxLen, uint8_t *txData, int32_t txLen)
{
    if (_pI2C->read(addr7 << 1, (char *)rxData, rxLen, false))
    {
        return -1;
    }
    return _pI2C->write(addr7 << 1, (char *)txData, txLen, true);
}