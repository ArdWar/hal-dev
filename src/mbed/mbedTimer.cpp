#include "mbedTimer.h"

satunol::MbedTimer::MbedTimer()
{
    _pMbedTicker = new ::Ticker();
}

satunol::MbedTimer::~MbedTimer()
{
    // delete _pLPTicker;
}

void satunol::MbedTimer::setPeriod(uint32_t msPeriod)
{
    _chronoPeriod = std::chrono::microseconds(1000 * msPeriod);
}

void satunol::MbedTimer::setHandler(TimerHandler *pHandlerObj)
{
    _pMbedCallback = callback(pHandlerObj, &TimerHandler::run);
}

void satunol::MbedTimer::setHandler(void (*pHandlerFunc)())
{
    _pMbedCallback = pHandlerFunc;
}

void satunol::MbedTimer::start()
{
    _pMbedTicker->attach(_pMbedCallback, _chronoPeriod);
}

void satunol::MbedTimer::stop()
{
    _pMbedTicker->detach();
}