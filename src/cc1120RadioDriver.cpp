#include "cc1120RadioDriver.hpp"

// Constructor & Destructor
Cc1120RadioDriver::Cc1120RadioDriver()
{
}

Cc1120RadioDriver::~Cc1120RadioDriver()
{
}

// Addtional Function Used by Core Function
unsigned char Cc1120RadioDriver::readRxFirst()
{
    unsigned char RxFirst = readRegSingle(ExtAddr, CC112X_RXFIRST);
    return RxFirst;
}
unsigned char Cc1120RadioDriver::readRxLast()
{
    unsigned char RxLast = readRegSingle(ExtAddr, CC112X_RXLAST);
    return RxLast;
}
void Cc1120RadioDriver::configSyncWord() {}

// Core Function
void Cc1120RadioDriver::initCc112x(unsigned char mosi, unsigned char miso, unsigned char clk, unsigned char cs)
{
    /* mrtInit for mrtDelay to works */
    // mrtInit(SYSTEM_CORE_CLOCK / 1000);
    /* spi Init for MISO, MOSI pin */
    SEGGER_RTT_printf(0, "initEntry()\n");
    spi.initCsPin(cs);
    spi.deactivateDevice(cs);
    spi.setPin(mosi, miso, clk);
    spi.setFrequency(1000000);
    spi.setMode(0);
    SEGGER_RTT_printf(0, "spisetupdone()\n");
    spi.init();

    int32_t del = 0;
    while (del++ < 300000)
        ;
    // mrtDelay(50); // this wait needs in order to prepare the cc1120 chip
    // spi.init(mosi, miso, clk);
    SSEL_PIN = cs; // give cs pin number to SSEL_PIN private function variable

    // while (del-- > 0)
    //     ;
    // mrtDelay(200); // this wait needs to wait for chip to set crystal to be stabilized
    /* Resetting cc1120 */
    // mrtDelay(20); // Prepare chip before resetting
    SEGGER_RTT_printf(0, "initDoing()\n");
    writeRegSingle(CmdStrobe, CC112X_SRES, 0x00);

    while (del++ < 300000)
        ;
    // mrtDelay(1); // wait for MISO tobe low after resetting
    SEGGER_RTT_printf(0, "initDone()\n");
    spi.activateDevice(SSEL_PIN);
    spi.deactivateDevice(SSEL_PIN);
    spi.activateDevice(SSEL_PIN);
    spi.deactivateDevice(SSEL_PIN);
}
void Cc1120RadioDriver::deinitCc112x() {}
void Cc1120RadioDriver::cc112xRegisterSet()
{
    unsigned char buff;
    unsigned char writeByte;
    unsigned short i;
    // Write registers to radio
    for (i = 0;
         i < (sizeof(preferredSettings) / sizeof(registerSetting_t)); i++)
    {
        writeByte = preferredSettings[i].data;
        buff = preferredSettings[i].addr >> 8;
        if (buff == 0x00)
            writeRegSingle(StdRegSpace, writeByte, preferredSettings[i].addr);
        else if (buff == 0x2F)
            writeRegSingle(ExtAddr, writeByte, preferredSettings[i].addr);
        else // Error
        {
        }
    }
    spi.activateDevice(SSEL_PIN);
    spi.deactivateDevice(SSEL_PIN);
    spi.activateDevice(SSEL_PIN);
    spi.deactivateDevice(SSEL_PIN);
}
unsigned char Cc1120RadioDriver::cc112xRegisterCheck()
{
    unsigned char buff;
    unsigned char readByte;
    unsigned char cmpByte;
    unsigned short i;

    // Read registers to radio
    for (i = 0;
         i < (sizeof(preferredSettings) / sizeof(registerSetting_t)); i++)
    {
        readByte = 0x00;
        cmpByte = preferredSettings[i].data;
        buff = preferredSettings[i].addr >> 8;
        if (buff == 0x00)
            readByte = readRegSingle(StdRegSpace, preferredSettings[i].addr);
        else if (buff == 0x2F)
            readByte = readRegSingle(ExtAddr, preferredSettings[i].addr);
        else // Error
        {
        }
        if (readByte != cmpByte)
        {
            return 1;
        }
    }
    return 0;
}
void Cc1120RadioDriver::manualCalibaration()
{
    unsigned char original_fs_cal2;
    unsigned char calResults_for_vcdac_start_high[3];
    unsigned char calResults_for_vcdac_start_mid[3];
    unsigned char marcstate;
    unsigned char writeByte;

    // 1) Set VCO cap-array to 0 (FS_VCO2 = 0x00)
    writeByte = 0x00;
    writeRegSingle(ExtAddr, writeByte, CC112X_FS_VCO2);

    // 2) Start with high VCDAC (original VCDAC_START + 2):
    original_fs_cal2 = readRegSingle(ExtAddr, CC112X_FS_CAL2);
    writeByte = original_fs_cal2 + VCDAC_START_OFFSET;
    writeRegSingle(ExtAddr, writeByte, CC112X_FS_CAL2);

    // 3) Calibrate and wait for calibration to be done
    //   (radio back in IDLE state)
    writeRegSingle(CmdStrobe, CC112X_SCAL, 0x00);
    do
    {
        marcstate = readRegSingle(ExtAddr, CC112X_MARCSTATE);
    } while (marcstate != 0x41);

    // 4) Read FS_VCO2, FS_VCO4 and FS_CHP register obtained with
    //    high VCDAC_START value
    calResults_for_vcdac_start_high[FS_VCO2_INDEX] = readRegSingle(ExtAddr, CC112X_FS_VCO2);
    calResults_for_vcdac_start_high[FS_VCO4_INDEX] = readRegSingle(ExtAddr, CC112X_FS_VCO4);
    calResults_for_vcdac_start_high[FS_CHP_INDEX] = readRegSingle(ExtAddr, CC112X_FS_CHP);

    // 5) Set VCO cap-array to 0 (FS_VCO2 = 0x00)
    writeByte = 0x00;
    writeRegSingle(ExtAddr, writeByte, CC112X_FS_VCO2);

    // 6) Continue with mid VCDAC (original VCDAC_START):
    writeByte = original_fs_cal2;
    writeRegSingle(ExtAddr, writeByte, CC112X_FS_CAL2);

    // 7) Calibrate and wait for calibration to be done
    //   (radio back in IDLE state)
    writeRegSingle(CmdStrobe, CC112X_SCAL, 0x00);

    do
    {
        marcstate = readRegSingle(ExtAddr, CC112X_MARCSTATE);
    } while (marcstate != 0x41);

    // 8) Read FS_VCO2, FS_VCO4 and FS_CHP register obtained
    //    with mid VCDAC_START value
    calResults_for_vcdac_start_mid[FS_VCO2_INDEX] = readRegSingle(ExtAddr, CC112X_FS_VCO2);
    calResults_for_vcdac_start_mid[FS_VCO4_INDEX] = readRegSingle(ExtAddr, CC112X_FS_VCO4);
    calResults_for_vcdac_start_mid[FS_CHP_INDEX] = readRegSingle(ExtAddr, CC112X_FS_CHP);

    // 9) Write back highest FS_VCO2 and corresponding FS_VCO
    //    and FS_CHP result
    if (calResults_for_vcdac_start_high[FS_VCO2_INDEX] >
        calResults_for_vcdac_start_mid[FS_VCO2_INDEX])
    {
        writeByte = calResults_for_vcdac_start_high[FS_VCO2_INDEX];
        writeRegSingle(ExtAddr, writeByte, CC112X_FS_VCO2);
        writeByte = calResults_for_vcdac_start_high[FS_VCO4_INDEX];
        writeRegSingle(ExtAddr, writeByte, CC112X_FS_VCO4);
        writeByte = calResults_for_vcdac_start_high[FS_CHP_INDEX];
        writeRegSingle(ExtAddr, writeByte, CC112X_FS_CHP);
    }
    else
    {
        writeByte = calResults_for_vcdac_start_mid[FS_VCO2_INDEX];
        writeRegSingle(ExtAddr, writeByte, CC112X_FS_VCO2);
        writeByte = calResults_for_vcdac_start_mid[FS_VCO4_INDEX];
        writeRegSingle(ExtAddr, writeByte, CC112X_FS_VCO4);
        writeByte = calResults_for_vcdac_start_mid[FS_CHP_INDEX];
        writeRegSingle(ExtAddr, writeByte, CC112X_FS_CHP);
    }
}
unsigned char Cc1120RadioDriver::readStatus() // Read Current MARC State
{
    unsigned char status = readRegSingle(CmdStrobe, CC112X_SNOP);
    status = status & 0xF0;
    status = status >> 4;
    if (status == 0)
    { // IDLE
        status = 0;
    }
    if (status == 1)
    { // RX Mode
        status = 1;
    }
    if (status == 2)
    { // TX Mode
        status = 2;
    }
    if (status == 3)
    { // FSTXON (Fast TX ready)
        status = 3;
    }
    if (status == 4)
    { // Calibrate
        status = 4;
    }
    if (status == 5)
    { // Settling
        status = 5;
    }
    if (status == 6)
    { // RX FIFO ERROR
        status = 6;
    }
    if (status == 7)
    { // TX FIFO ERROR
        status = 7;
    }
    return status;
}
void Cc1120RadioDriver::flushRxFifo() { writeRegSingle(CmdStrobe, CC112X_SFRX, 0x00); } // SFRX Command Strobe
void Cc1120RadioDriver::flushTxFifo() { writeRegSingle(CmdStrobe, CC112X_SFTX, 0x00); } // SFTX Command Strobe
void Cc1120RadioDriver::send(unsigned char *pData, unsigned int len)
{
    unsigned int timeOut = 0;
    writeRegBurst(StdFIFO, pData, len, 0x00);
    writeRegSingle(CmdStrobe, CC112X_STX, 0x00);
    // mrtDelay(1);
    // Start Waiting for Command Strobe Completion
    unsigned char status = readStatus();
    while (status != 0 && timeOut != 2000)
    {
        status = readStatus();
        timeOut++;
        // mrtDelay(1);
    }
    // SEGGER_RTT_printf(0, "TimeOut Send is : %d miliseconds\n", timeOut);
    status = readStatus();
    if (status == 0)
    {
        // Packet Sent Successfull
    }
    else if (status == 2)
    {
        // TimeOut Reach, Sending Error
    }
    else
    {
        // Status error
    }
    // ---------------
    // mrtDelay(1);
    // mrtDelay(200);

    /* Read TX FIFO */
    // unsigned char buff = 0xFE;
    // unsigned char dataBuff[100];
    // spi.activateDevice(SSEL_PIN);
    // spi.write(&buff, 1); // command to read direct access FIFO
    // for (unsigned char a = 0; a < TxLast; a++)
    // {
    //     spi.write(&a,1);
    //     spi.recv(&dataBuff[a], 1);
    // }
    // spi.deactivateDevice(SSEL_PIN);
    // // SEGGER_RTT_printf(0, "0x%X", buff1);
    // SEGGER_RTT_printf(0, "dataBuff value is =\n");
    // for (unsigned char a = 0; a < len; a++)
    // {
    //     SEGGER_RTT_printf(0, "0x%X ", dataBuff[a]);
    // }
    // SEGGER_RTT_printf(0, "\n");
}
unsigned char Cc1120RadioDriver::recv(unsigned char *pData, unsigned char *pRssi, unsigned char *pLen, unsigned short *wakeUpIntv)
{
    writeRegSingle(CmdStrobe, CC112X_SRX, 0x00);
    // mrtDelay(1);
    unsigned int timeOut = 0;
    // Start Waiting for Command Strobe Completion
    unsigned char status = readStatus();
    // SEGGER_RTT_printf(0, "timeOutLimit Recv CC1120 is = %d mili secomds\n", *wakeUpIntv * 1000);
    while (status != 0 && timeOut != (*wakeUpIntv * 1000))
    {
        status = readStatus();
        timeOut++;
        // mrtDelay(1);
    }
    status = readStatus();
    if (status == 0)
    {
        // SEGGER_RTT_printf(0, "CC1120-RecvFunction-Data Received\n");
        // mrtDelay(1); //To Ensure received data are well written in cc1120 register
        // readPacket(pData, pLength, pRssi, pLQI, pCRC);
        unsigned char rxFirst = readRxFirst();
        unsigned char rxLast = readRxLast();
        readRegBurst(pData, rxFirst, rxLast);
        *pLen = rxLast;             // actual len data received from cc1120
        *pRssi = pData[rxLast - 1]; // pData[rxLast] = CRC and LQI info, while pData[rxLast -1] = RSSI (this 2 byte appended from cc1120 system)
        return 0;
        // Packet Receive Successfull
    }
    else if (status == 1)
    {
        // SEGGER_RTT_printf(0, "CC1120-RecvFunction-Timeout Reach\n");
        return 1;
        // TimeOut Reach, Receive Error
    }
    else
    {
        // SEGGER_RTT_printf(0, "CC1120-RecvFunction-Status ERROR\n");s
        return 2;
        // Status error
    }
    // ---------------

    /* DEBUG */
    // SEGGER_RTT_printf(0, "dataRecvBuff Value is =\n");
    // for (unsigned char a = 0; a <= rxLast; a++)
    //     SEGGER_RTT_printf(0, " 0x%X ", pData[a]);
}
void Cc1120RadioDriver::readPacket(unsigned char *pData, unsigned char *pLen, unsigned char *pRssi, unsigned char *pLQI, bool *pCrc)
{
    // unsigned char i;
    // unsigned char crclqi;
    // unsigned char buff = 0xFF;
    // unsigned char rxFirst = readRxFirst();
    // // unsigned char rxLast = readRxLast
    // unsigned char len[1];
    // // SEGGER_RTT_printf(0, "RX FIRST Value is : 0x%X\n", rxFirst);
    // spi.activateDevice(SSEL_PIN);
    // spi.write(&buff, 1); // read RX FIFO
    // spi.write(0x00, 1);
    // spi.recv(len, 1);
    // // SEGGER_RTT_printf(0, "data length Value is : 0x%X\n", len[0]);
    // for (i = 0; i < *pLen; i++)
    // {
    //     buff = i + 1;
    //     spi.write(&buff, 1);
    //     spi.recv(&pData[i], 1);
    // }
    // buff = i + 1;
    // spi.write(&buff, 1);
    // spi.recv(pRssi, 1);
    // buff++;
    // spi.write(&buff, 1);
    // spi.recv(&crclqi, 1);
    // *pCrc = crclqi & 0x80;
    // *pLQI = crclqi & 0x7f;
    // spi.deactivateDevice(SSEL_PIN);
}
void Cc1120RadioDriver::enterSleepMode()
{
    writeRegSingle(CmdStrobe, CC112X_SPWD, 0x00);
}

void Cc1120RadioDriver::quitSleepMode()
{
    // To Wake up from sleep (SPWD), cs pin need to be asserted (cs = 0)
    spi.activateDevice(SSEL_PIN);
}

// Set Up Function
void Cc1120RadioDriver::setSniffMode(float sleepTimeout) {} // Parameter Notset Yet
unsigned char Cc1120RadioDriver::setCarrierFreq(unsigned char freq)
{
    unsigned int freqCode;
    unsigned char dataBuff;
    freqCode = CC112X_FREQ_BASE + ((uint16_t)freq * CC112X_FREQ_STEP);
    // Set Freq Channel Register
    dataBuff = (freqCode >> 16) & 0xFF;
    writeRegSingle(ExtAddr, dataBuff, CC112X_FREQ2);
    dataBuff = (freqCode >> 8) & 0xFF;
    writeRegSingle(ExtAddr, dataBuff, CC112X_FREQ1);
    dataBuff = freqCode & 0xFF;
    writeRegSingle(ExtAddr, dataBuff, CC112X_FREQ0);
    // Read Freq Channell Register
    dataBuff = readRegSingle(ExtAddr, CC112X_FREQ2);
    if (dataBuff != ((freqCode >> 16) & 0xFF))
        return 1;
    dataBuff = readRegSingle(ExtAddr, CC112X_FREQ1);
    if (dataBuff != ((freqCode >> 8) & 0xFF))
        return 1;
    dataBuff = readRegSingle(ExtAddr, CC112X_FREQ0);
    if (dataBuff != (freqCode & 0xFF))
        return 1;
    return 0;
}
void Cc1120RadioDriver::setChipAddress(unsigned char address) {}
void Cc1120RadioDriver::setSyncWord(unsigned int value) {} //SYNC WORD Value (32-bit) default 0x930B51DE
void Cc1120RadioDriver::setTxPower(unsigned char txPower) {}

// Debug Function
void Cc1120RadioDriver::debugModeRegisterCheck()
{
    unsigned char buff = readRegSingle(StdRegSpace, CC112X_IOCFG0);
    SEGGER_RTT_printf(0, "CC112X_IOCFG0 value is : 0x%X\n", buff);
    buff = readRegSingle(StdRegSpace, CC112X_PA_CFG0);
    SEGGER_RTT_printf(0, "CC112X_PA_CFG0 value is : 0x%X\n", buff);
    buff = readRegSingle(ExtAddr, CC112X_FREQ2);
    SEGGER_RTT_printf(0, "CC112X_FREQ2 value is : 0x%X\n", buff);
    buff = readRegSingle(ExtAddr, CC112X_XOSC5);
    SEGGER_RTT_printf(0, "CC112X_XOSC5 value is : 0x%X\n", buff);
}

/* Write & Read Function */
void Cc1120RadioDriver::writeRegSingle(RegAcces access, unsigned char Value, unsigned short Addr)
{
    unsigned char AddrBuff;
    if (access == StdRegSpace)
    {
        AddrBuff = Addr & 0xFF;
        if (AddrBuff < 0x2F)
        {
            spi.activateDevice(SSEL_PIN);
            spi.write(&AddrBuff, 1);
            spi.write(&Value, 1);
            spi.deactivateDevice(SSEL_PIN);
        }
        else
        {
            // Error
        }
    }
    else if (access == ExtAddr)
    {
        AddrBuff = Addr & 0xFF;
        unsigned char buff = 0x2F;
        spi.activateDevice(SSEL_PIN);
        spi.write(&buff, 1);
        spi.write(&AddrBuff, 1);
        spi.write(&Value, 1);
        spi.deactivateDevice(SSEL_PIN);
    }
    else if (access == CmdStrobe)
    {
        if (Value >= 0x30 && Value <= 0x3D)
        {
            spi.activateDevice(SSEL_PIN);
            spi.write(&Value, 1);
            spi.deactivateDevice(SSEL_PIN);
        }
        else
        {
            // serial->printf("error while accesssing command strobe\n");
            // Error
        }
    }
}

void Cc1120RadioDriver::writeRegBurst(RegAcces access, unsigned char *pData, unsigned char length, unsigned short Addr)
{
    unsigned char AddrBuff;
    if (access == StdRegSpace)
    {
        AddrBuff = Addr & 0xFF;
        if (AddrBuff < 0x2F)
        {
            unsigned char buff = AddrBuff | 0x40;
            spi.activateDevice(SSEL_PIN);
            spi.write(&buff, 1);
            for (int i = 0; i < length; i++)
            {
                spi.write(&pData[i], 1);
            }
            spi.deactivateDevice(SSEL_PIN);
        }
        else
        {
            // Error
        }
    }
    else if (access == ExtAddr)
    {
        AddrBuff = Addr & 0xFF;
        unsigned char buff = AddrBuff | 0x40;
        unsigned char buff2 = 0x2F;
        spi.activateDevice(SSEL_PIN);
        spi.write(&buff2, 1);
        spi.write(&buff, 1);
        for (int i = 0; i < length; i++)
        {
            spi.write(&pData[i], 1);
        }
        spi.deactivateDevice(SSEL_PIN);
    }
    else if (access == StdFIFO)
    {
        if (length <= 0x7F) // Length not more than 0x7F or 128
        {
            unsigned char buff = 0x7F;
            spi.activateDevice(SSEL_PIN);
            spi.write(&buff, 1);
            spi.write(&length, 1);
            for (int i = 0; i < length; i++)
            {
                spi.write(&pData[i], 1);
            }
            spi.deactivateDevice(SSEL_PIN);
        }
        else
        {
            // Error
        }
    }
}

unsigned char Cc1120RadioDriver::readRegSingle(RegAcces access, unsigned short Addr)
{
    unsigned char AddrBuff;
    if (access == StdRegSpace)
    {
        AddrBuff = Addr & 0xFF;
        if (AddrBuff < 0x2F)
        {
            unsigned char buff = AddrBuff | 0x80;
            unsigned char dataRecvBuff[1];
            spi.activateDevice(SSEL_PIN);
            spi.write(&buff, 1);
            spi.read(dataRecvBuff, 1);
            spi.deactivateDevice(SSEL_PIN);
            return dataRecvBuff[0];
        }
        else
        {
            // Error
        }
    }
    else if (access == ExtAddr)
    {
        AddrBuff = Addr & 0xFF;
        unsigned char buff = 0xAF;
        unsigned char dataRecvBuff[1];
        spi.activateDevice(SSEL_PIN);
        spi.write(&buff, 1);
        spi.write(&AddrBuff, 1);
        spi.read(dataRecvBuff, 1);
        spi.deactivateDevice(SSEL_PIN);
        return dataRecvBuff[0];
    }
    else if (access == CmdStrobe)
    {
        AddrBuff = Addr & 0xFF;
        unsigned char dataRecvBuff[1];
        spi.activateDevice(SSEL_PIN);
        spi.write(&AddrBuff, 1);
        spi.read(dataRecvBuff, 1);
        spi.deactivateDevice(SSEL_PIN);
        return dataRecvBuff[0];
    }
    return 0;
}

void Cc1120RadioDriver::readRegBurst(unsigned char *pData, unsigned char rxFirst, unsigned char rxLast) // Just for Standard RX FIFO
{
    // SEGGER_RTT_printf(0, "rxFirst Value is = 0x%X\n", rxFirst);
    // SEGGER_RTT_printf(0, "readRxLast Value is = 0x%X\n", rxLast);
    unsigned char buff = 0xFF; // Read RXFIFO
    // unsigned char dataRecvBuff[15];
    spi.activateDevice(SSEL_PIN);
    spi.write(&buff, 1); // read RX FIFO
    for (unsigned char a = rxFirst; a <= rxLast; a++)
    {
        unsigned char recvBuff = a;
        spi.read(&recvBuff, 1);
        // dataRecvBuff[a] = recvBuff;
        pData[a] = recvBuff;
    }
    // spi.recv(dataRecvBuff, 1);
    spi.deactivateDevice(SSEL_PIN);
    // SEGGER_RTT_printf(0, "dataRecvBuff Value is =\n");
    // for (unsigned char a = 0; a <= rxLast; a++)
    //     SEGGER_RTT_printf(0, " 0x%X ", dataRecvBuff[a]);
    // rxFirst = readRxFirst();
    // rxLast = readRxLast();
    // SEGGER_RTT_printf(0, "\nrxFirst Value is = 0x%X\n", rxFirst);
    // SEGGER_RTT_printf(0, "readRxLast Value is = 0x%X\n", rxLast);
}