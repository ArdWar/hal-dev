#include "lpc8xxSpiMasterDriver.h"
#include "SEGGER_RTT.h"

Lpc8xxSpiMasterDriver::Lpc8xxSpiMasterDriver()
{
}

Lpc8xxSpiMasterDriver::~Lpc8xxSpiMasterDriver()
{
}

void Lpc8xxSpiMasterDriver::setPin(uint32_t mosi, uint32_t miso, uint32_t sclk)
{
    _SPI_MOSI = mosi;
    _SPI_MISO = miso;
    _SPI_SCLK = sclk;
}

void Lpc8xxSpiMasterDriver::initCsPin(uint32_t ssel)
{
    LPC_GPIO_PORT->DIR0 |= (1 << ssel);
    LPC_GPIO_PORT->SET0 |= (1 << ssel);
}

void Lpc8xxSpiMasterDriver::setFrequency(uint32_t freq)
{
    _SPI_FREQ = freq;
}

void Lpc8xxSpiMasterDriver::setMode(uint8_t mode)
{
    _SPI_MODE = mode;
}

int32_t Lpc8xxSpiMasterDriver::init()
{
    // enable SwitchMatrix and SPI0 clock
    LPC_SYSCON->SYSAHBCLKCTRL |= SWM_CLK | SPI0_CLK;

    // assign pin to SPI peripheral
    LPC_SWM->PINASSIGN[4] = (LPC_SWM->PINASSIGN[4] & 0xFFFF0000) | _SPI_MOSI | (_SPI_MISO << 8);
    LPC_SWM->PINASSIGN[3] = (LPC_SWM->PINASSIGN[3] & 0x00FFFFFF) | (_SPI_SCLK << 24);

    // LPC_SWM->PINENABLE0 |= 1 << 8;

    // disable SwitchMatrix clock
    LPC_SYSCON->SYSAHBCLKCTRL &= ~SWM_CLK;

    // reset SPI peripheral
    LPC_SYSCON->PRESETCTRL &= ~SPI0_RST;
    LPC_SYSCON->PRESETCTRL |= SPI0_RST;

    // enable SPI peripheral, set as master, set SPI mode
    LPC_SPI0->CFG |= (uint32_t)(SPI_CFG_ENABLE | SPI_CFG_MASTER | (_SPI_MODE & 0x3) << 4);

    // set SPI frequency
    LPC_SPI0->DIV = (32000000 / _SPI_FREQ) - 1;

    // do not use inbuilt SSEL, set 8-bit data per frame
    LPC_SPI0->TXCTL = SPI_TX_SSELN | SPI_TX_LEN8BIT;
    return 0;
}

void Lpc8xxSpiMasterDriver::deinit()
{
    // enable SwitchMatrix clock
    LPC_SYSCON->SYSAHBCLKCTRL |= SWM_CLK;

    // unassign pin from SPI peripheral
    LPC_SWM->PINASSIGN[4] = (LPC_SWM->PINASSIGN[4] & 0xFFFF0000) | 0x0000FFFF;
    LPC_SWM->PINASSIGN[3] = (LPC_SWM->PINASSIGN[3] & 0x00FFFFFF) | 0xFF000000;

    // disable SwitchMatrix and SPI clock
    LPC_SYSCON->SYSAHBCLKCTRL &= ~(SWM_CLK | SPI0_CLK);
}

/**************************************************/
/*  All SPI transmission is essentially the same  */
/*  Since it's a full duplex communication        */
/**************************************************/

int32_t Lpc8xxSpiMasterDriver::write(uint8_t *txData, int32_t txLen)
{
    return writeread(txData, txLen, 0, 0);
}

int32_t Lpc8xxSpiMasterDriver::read(uint8_t *rxData, int32_t rxLen)
{
    return writeread(0, 0, rxData, rxLen);
}

int32_t Lpc8xxSpiMasterDriver::writeread(uint8_t *txData, int32_t txLen, uint8_t *rxData, int32_t rxLen)
{
    int32_t len = (rxLen > txLen) ? rxLen : txLen;
    while (len > 0)
    {
        // wait until ready to transmit
        while (!(LPC_SPI0->STAT & SPI_STAT_TXRDY))
            ;
        // put End Of Transfer flag if it's the last byte to send/receive (whichever is longer)
        LPC_SPI0->TXCTL |= (len-- <= 1) ? SPI_TX_EOT : 0;

        // put data to TX register if it's still within txLen, otherwise put zeros
        LPC_SPI0->TXDAT = (txLen-- > 0) ? ((uint32_t)*txData++ & 0xFFFF) : 0;

        // wait until rxdata ready to read
        while (!(LPC_SPI0->STAT & SPI_STAT_RXRDY))
            ;

        // read received data
        uint8_t rcvd = (uint8_t)(LPC_SPI0->RXDAT & 0xFF);

        // if still within rx data length...
        if (rxLen-- > 0)
        {
            // ...put data into buffer...
            *rxData++ = rcvd;
        } // ...otherwise discard it
    }
    return 0;
}

void Lpc8xxSpiMasterDriver::activateDevice(uint32_t ssel)
{
    LPC_GPIO_PORT->CLR0 |= (1 << ssel);
}

void Lpc8xxSpiMasterDriver::deactivateDevice(uint32_t ssel)
{
    LPC_GPIO_PORT->SET0 |= (1 << ssel);
}