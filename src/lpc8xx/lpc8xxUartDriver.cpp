#include "lpc8xxUartDriver.h"

Lpc8xxUartDriver::Lpc8xxUartDriver()
{
}

Lpc8xxUartDriver::~Lpc8xxUartDriver()
{
}

uint32_t Lpc8xxUartDriver::setPin(uint32_t txd = 0xff, uint32_t rxd = 0xff)
{
    _UART_TXD = txd;
    _UART_RXD = rxd;
    return 0;
}
uint32_t Lpc8xxUartDriver::setConfig(uint32_t baudRate = 9600, uint8_t frameSize = 8, uint8_t parityBit = 0, uint8_t stopBit = 1)
{
    _UART_CONFIG.sys_clk_in_hz = SystemCoreClock;
    _UART_CONFIG.baudrate_in_hz = baudRate;
    _UART_CONFIG.config = (frameSize == 7 ? 0 : 1) | (parityBit == 1 ? (3 << 2) : (parityBit << 2)) | ((stopBit - 1) << 4);
    _UART_CONFIG.sync_mod = 0;
    _UART_CONFIG.error_en = 0;
    return 0;
}

uint32_t Lpc8xxUartDriver::setSwFlowControl()
{
    return 0;
}

uint32_t Lpc8xxUartDriver::setHwFlowControl(uint32_t rts = 0xff, uint32_t cts = 0xff)
{
    _UART_RTS = rts;
    _UART_CTS = cts;
    return 0;
}

uint32_t Lpc8xxUartDriver::init()
{
    LPC_SYSCON->SYSAHBCLKCTRL |= UART0_CLK | SWM_CLK;
    LPC_SWM->PINASSIGN[0] = (LPC_SWM->PINASSIGN[0] & 0xFFFF0000) | _UART_TXD | (_UART_RXD << 8);
    LPC_SYSCON->SYSAHBCLKCTRL &= ~SWM_CLK;
    LPC_SYSCON->UARTCLKDIV = 2;

    LPC_SYSCON->PRESETCTRL &= ~(UARTFRG_RST_N | UART0_RST_N | UART1_RST_N);
    LPC_SYSCON->PRESETCTRL |= UARTFRG_RST_N | UART0_RST_N | UART1_RST_N;

    uartHandle = LPC_UARTD_API->uart_setup(LPC_USART0_BASE, uartHandleMEM);

    LPC_UARTD_API->uart_init(uartHandle, &_UART_CONFIG);
    return 0;
}

uint32_t Lpc8xxUartDriver::deinit()
{
    LPC_SYSCON->SYSAHBCLKCTRL &= ~UART0_CLK;
    return 0;
}

uint32_t Lpc8xxUartDriver::putChar(char c)
{
    LPC_UARTD_API->uart_put_char(uartHandle, c);
    while ((LPC_USART0->STAT & STAT_TXIDLE) == 0)
        ;
    return 0;
}

uint32_t Lpc8xxUartDriver::putLine(const char *str)
{
    UART_PARAM_T param;
    param.buffer = str;
    param.size = strlen(str);
    param.trm_mode = UART_TRM_MODE_T::NULLTERM;
    param.drv_mode = UART_DRV_MODE_T::POLL;

    LPC_UARTD_API->uart_put_line(uartHandle, &param);
    while ((LPC_USART0->STAT & STAT_TXIDLE) == 0)
        ;
    return 0;
}

char Lpc8xxUartDriver::getChar()
{
    return LPC_UARTD_API->uart_get_char(uartHandle);
}

uint32_t Lpc8xxUartDriver::getLine(char *str, uint32_t len)
{
    // Not working yet
    UART_PARAM_T param;
    param.buffer = str;
    param.size = 255;
    param.trm_mode = UART_TRM_MODE_T::NULLTERM;
    param.drv_mode = UART_DRV_MODE_T::POLL;

    while ((LPC_USART0->STAT & 1) != 1)
        ;
    LPC_UARTD_API->uart_get_line(uartHandle, &param);
    while ((LPC_USART0->STAT & (1 << 1)) == 0)
        ;
    return 0;
}

uint32_t Lpc8xxUartDriver::dataAvailable()
{
    return 0;
}

uint32_t Lpc8xxUartDriver::powerUp(uint8_t state)
{
    if (state)
    {
        init();
    }
    else
    {
        deinit();
    }
    return 0;
}