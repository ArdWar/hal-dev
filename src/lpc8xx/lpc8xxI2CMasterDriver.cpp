/**
 * @file    lpc8xxI2CMasterDriver.cpp
 * @author  www.satunol.com
 * @brief   I2C driver for lpc8xx devices
 * @version 0.5
 * @date    2020-07-16
 * 
 * @copyright Copyright (c) 2020
 */

#include "lpc8xxI2CMasterDriver.h"

Lpc8xxI2CMasterDriver::Lpc8xxI2CMasterDriver()
{
}

Lpc8xxI2CMasterDriver::~Lpc8xxI2CMasterDriver()
{
}

void Lpc8xxI2CMasterDriver::setPin(uint32_t sda, uint32_t scl)
{
    _I2C_SDA = sda;
    _I2C_SCL = scl;
}

void Lpc8xxI2CMasterDriver::setFrequency(uint32_t clk)
{
    _I2C_CLK = clk;
}

void Lpc8xxI2CMasterDriver::active(bool isActive)
{
    if (isActive)
    {
        // activate I2C clock
        LPC_SYSCON->SYSAHBCLKCTRL |= I2C_CLK;
    }
    else
    {
        // deactivate I2C clock
        LPC_SYSCON->SYSAHBCLKCTRL &= ~I2C_CLK;
    }
}

int32_t Lpc8xxI2CMasterDriver::init()
{
    // activate I2C and SwitchMatrix clock
    LPC_SYSCON->SYSAHBCLKCTRL |= (I2C_CLK | SWM_CLK);

    // assign pin to I2C peripheral
    LPC_SWM->PINASSIGN[7] = (LPC_SWM->PINASSIGN[7] & 0x00FFFFFF) | (_I2C_SDA << 24);
    LPC_SWM->PINASSIGN[8] = (LPC_SWM->PINASSIGN[8] & 0xFFFFFF00) | _I2C_SCL;

    // deactivate SwitchMatrix clock
    LPC_SYSCON->SYSAHBCLKCTRL &= ~SWM_CLK;

    // reset I2C periperal registers
    LPC_SYSCON->PRESETCTRL &= ~I2C_RST;
    LPC_SYSCON->PRESETCTRL |= I2C_RST;

    // create handle for I2C_ROM_API 
    i2cHandle = LPC_I2CD_API->i2c_setup(LPC_I2C_BASE, i2cHandleMEM);

    // set I2C bitrate, return error if fail to do so
    if (LPC_I2CD_API->i2c_set_bitrate(i2cHandle, SystemCoreClock, _I2C_CLK))
    {
        return -1;
    }

    // set I2C timeout, return error if fail to do so
    return LPC_I2CD_API->i2c_set_timeout(i2cHandle, 65535) == 0 ? 0 : -1;
}

void Lpc8xxI2CMasterDriver::deinit()
{
    // activate SwitchMatrix clock
    LPC_SYSCON->SYSAHBCLKCTRL |= SWM_CLK;

    // unassign pin from I2C peripheral
    LPC_SWM->PINASSIGN[7] = (LPC_SWM->PINASSIGN[7] & 0x00FFFFFF) | 0xFF000000;
    LPC_SWM->PINASSIGN[8] = (LPC_SWM->PINASSIGN[8] & 0xFFFFFF00) | 0x000000FF;

    // deactivate I2C and SwitchMatrix clock
    LPC_SYSCON->SYSAHBCLKCTRL &= ~(SWM_CLK | I2C_CLK);
}

int32_t Lpc8xxI2CMasterDriver::write(uint8_t addr7, uint8_t *txData, int32_t txLen)
{
    I2C_PARAM_T param;

    // create temporary TX buffer since I2C_ROM_API require addr to be included
    uint8_t tempTxData[MAXDATALENGTH];

    param.num_bytes_send = ++txLen;
    param.buffer_ptr_send = &tempTxData[0];
    param.stop_flag = 1;

    // put addr to first byte, put data to following bytes
    *tempTxData = addr7 << 1;
    while (--txLen >= 0)
    {
        tempTxData[txLen + 1] = txData[txLen];
    }

    // transmit data, return error if fail to do so
    return LPC_I2CD_API->i2c_master_transmit_poll(i2cHandle, &param, 0) == 0 ? 0 : -1;
}

int32_t Lpc8xxI2CMasterDriver::read(uint8_t addr7, uint8_t *rxData, int32_t rxLen)
{
    I2C_PARAM_T param;

    // create temporary RX buffer since I2C_ROM_API require addr to be included
    uint8_t tempRxData[MAXDATALENGTH];

    // put addr to first byte
    *tempRxData = addr7 << 1;

    param.num_bytes_rec = ++rxLen;
    param.buffer_ptr_rec = &tempRxData[0];
    param.stop_flag = 1;

    // receive data, return error if fail to do so
    if (LPC_I2CD_API->i2c_master_receive_poll(i2cHandle, &param, 0)) {
        return -1;
    }

    // put received data from temporary buffer to RX buffer
    while (--rxLen >= 0)
    {
        rxData[rxLen] = tempRxData[rxLen + 1];
    }

    return 0;
}

int32_t Lpc8xxI2CMasterDriver::writeread(uint8_t addr7, uint8_t *txData, int32_t txLen, uint8_t *rxData, int32_t rxLen)
{
    I2C_PARAM_T param;

    // create temporary buffers since I2C_ROM_API require addr to be included
    uint8_t tempTxData[MAXDATALENGTH];
    uint8_t tempRxData[MAXDATALENGTH];

    // put addr to first byte of each buffer
    *tempTxData = addr7 << 1;
    *tempRxData = addr7 << 1;

    param.num_bytes_rec = ++rxLen;
    param.num_bytes_send = ++txLen;

    // put TX buffer data to temporary TX buffer
    while (--txLen >= 0)
    {
        tempTxData[txLen + 1] = txData[txLen];
    }

    param.buffer_ptr_send = &tempTxData[0];
    param.buffer_ptr_rec = &tempRxData[0];
    param.stop_flag = 1;

    // transmit then receive, return error if fail to do so
    if (LPC_I2CD_API->i2c_master_tx_rx_poll(i2cHandle, &param, 0)) {
        return -1;
    }

    // recover received data from temp buffer to RX buffer
    while (--rxLen >= 0)
    {
        rxData[rxLen] = tempRxData[rxLen + 1];
    }

    return 0;
}

int32_t Lpc8xxI2CMasterDriver::readwrite(uint8_t addr7, uint8_t *rxdata, int32_t rxlen, uint8_t *txdata, int32_t txlen)
{
    return -1; // this function is not implemented on LPC8xx
}