#ifndef _UARTDRIVER_H_
#define _UARTDRIVER_H_

#include <stdint.h>

class UartDriver
{
public:
    virtual uint32_t setPin(uint32_t txd, uint32_t rxd) = 0;
    virtual uint32_t setConfig(uint32_t baudRate, uint8_t frameSize, uint8_t parityBit, uint8_t stopBit) = 0;
    virtual uint32_t setSwFlowControl() = 0;
    virtual uint32_t setHwFlowControl(uint32_t rts, uint32_t cts) = 0;

    virtual uint32_t init() = 0;
    virtual uint32_t deinit() = 0;

    virtual uint32_t putChar(char c) = 0;
    virtual uint32_t putLine(const char *str) = 0;
    virtual char getChar() = 0;
    virtual uint32_t getLine(char *str, uint32_t len) = 0;
    virtual uint32_t dataAvailable() = 0;
    virtual uint32_t powerUp(uint8_t state) = 0;
};

#endif