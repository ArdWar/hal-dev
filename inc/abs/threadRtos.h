#ifndef _THREADRTOS_HPP_
#define _THREADRTOS_HPP_

namespace satunol
{
    class ThreadHandler
    {
    public:
        virtual void run() = 0;
    };

    class Thread
    {
    public:
        static Thread *getCurrentThread(); //  ++ tambahan // untuk mendapatkan konteks dimana dia running

        virtual void setPriority(uint8_t threadPriority) = 0;
        virtual void setStackSize(uint32_t stackSize) = 0;

        virtual void start() = 0;
        virtual void stop() = 0;                   // kalo implementasi nya gak nemu, di return langsung aja
        virtual void sleep(uint32_t msPeriod) = 0; // ++ tambahan

        virtual void setHandler(ThreadHandler *pHandlerObj);
        virtual void setHandler(void (*pHandlerFunc)());

        // private:
        //     ThreadHandler *_pHandlerObj;
    };
} // namespace satunol
#endif