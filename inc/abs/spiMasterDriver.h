#ifndef _SPIMASTERDRIVER_H_
#define _SPIMASTERDRIVER_H_

#include <stdint.h>

class SpiMasterDriver
{
public:
    virtual void setPin(uint32_t mosi, uint32_t miso, uint32_t sclk) = 0;
    virtual void setMode(uint8_t mode);
    virtual void setFrequency(uint32_t freq) = 0;

    virtual void initCsPin(uint32_t ssel) = 0;

    virtual int32_t init() = 0;
    virtual void deinit() = 0;

    virtual int32_t write(uint8_t *txData, uint8_t txLen) = 0;
    virtual int32_t read(uint8_t *rxData, uint8_t rxLen) = 0;
    virtual int32_t writeread(uint8_t *txData, uint8_t txLen, uint8_t *rxData, uint8_t rxLen) = 0;

    virtual void activateDevice(uint32_t ssel) = 0;
    virtual void deactivateDevice(uint32_t ssel) = 0;
};

#endif