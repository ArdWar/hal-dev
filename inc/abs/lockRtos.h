#ifndef _LOCKRTOS_H_
#define _LOCKRTOS_H_

namespace satunol
{
    class Lock
    {
    public:
        virtual void lock() = 0;
        virtual void unlock() = 0;
    };
} // namespace satunol
#endif