#ifndef _TIMERRTOS_H_
#define _TIMERRTOS_H_

namespace satunol
{
    class TimerHandler
    {
    public:
        virtual void run() = 0;
    };

    class Timer
    {
    public:
        virtual void setPeriod(uint32_t msPeriod) = 0;
        virtual void setHandler(TimerHandler *pHandlerObj) = 0;
        virtual void setHandler(void (*pHandlerFunc)()) = 0;

        virtual void start() = 0;
        virtual void stop() = 0;

        // private:
        //     TimerHandler *_pHandlerObj;
    };
} // namespace satunol
#endif