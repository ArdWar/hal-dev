#ifndef _I2CMASTERDRIVER_H_
#define _I2CMASTERDRIVER_H_

#include <stdint.h>

class I2CMasterDriver
{
private:
public:
    /**
     * @brief           Set I2C peripheral pinout object. Pin is *not* actually assigned until init()
     * 
     * @param sda       SDA Pin
     * @param scl       SCL Pin
     */
    virtual void setPin(uint32_t sda, uint32_t scl) = 0;

    /**
     * @brief           Set I2C clock rate. Clock is *not* actually set until init() 
     * 
     * @param clk       Clock rate in Hz
     */
    virtual void setFrequency(uint32_t clk) = 0;

    /**
     * @brief           Activate/deactivate I2C peripheral clock.
     * 
     * @param isActive  true = active, false = inactive
     */
    virtual void active(bool isActive) = 0;

    /**
     * @brief           Initialize I2C peripheral. Allocate pinout and set parameters.
     * 
     * @return int32_t  0 if success
     */
    virtual int32_t init() = 0;

    /**
     * @brief           Deinitialize I2C peripheral. Deactivate clock and release the pins.
     */
    virtual void deinit() = 0;

    /**
     * @brief           Write data to addressed I2C slave
     * 
     * @param addr7     7-bit address of the target slave
     * @param txData    input data to be sent
     * @param txLen     length of the data
     * @return int32_t  0 if success
     */
    virtual int32_t write(uint8_t addr7, uint8_t *txData, int32_t txLen) = 0;

    /**
     * @brief           Read data from addressed I2C slave
     * 
     * @param addr7     7-bit address of the target slave
     * @param rxData    output data sent by slave
     * @param rxLen     length of the data
     * @return int32_t  0 if success
     */
    virtual int32_t read(uint8_t addr7, uint8_t *rxData, int32_t rxLen) = 0;

    /**
     * @brief           Write data then immediately read data by using repeat start
     * 
     * @param addr7     7-bit address of the target slave
     * @param txData    data to be written to slave
     * @param txLen     length of data to be written
     * @param rxData    data to be read from the slave
     * @param rxLen     length of data to be read
     * @return int32_t  0 if success
     */
    virtual int32_t writeread(uint8_t addr7, uint8_t *txData, int32_t txLen, uint8_t *rxData, int32_t rxLen) = 0;

    /**
     * @brief           Read data then immediately write data by using repeat start. *Not every platform support this method*.
     * 
     * @param addr7     7-bit address of the target slave
     * @param rxData    data to be read from the slave
     * @param rxLen     length of data to be read
     * @param txData    data to be written to slave
     * @param txLen     length of data to be written
     * @return int32_t  0 if success
     */
    virtual int32_t readwrite(uint8_t addr7, uint8_t *rxData, int32_t rxLen, uint8_t *txData, int32_t txLen) = 0;
};

#endif