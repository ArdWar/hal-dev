#ifndef __MAIN_H_
#define __MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include "LPC8xx.h"
#include "SEGGER_RTT.h"
#include "lpc8xxI2CMasterDriver.h"
#include "lpc8xxSpiMasterDriver.h"
#include "lpc8xxUartDriver.h"
#include "cc1120RadioDriver.hpp"

#endif