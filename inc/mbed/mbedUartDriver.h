#ifndef _MBEDUARTDRIVER_H_
#define _MBEDUARTDRIVER_H_

#include "mbed.h"
#include "uartDriver.h"

class MbedUartDriver : public UartDriver
{
private:
    PinName _UART_TXD;
    PinName _UART_RXD;

    PinName _UART_FLOW1;
    PinName _UART_FLOW2;

    uint32_t _UART_BAUD;
    uint8_t _UART_BITS;
    BufferedSerial::Parity _UART_PARITY;
    uint8_t _UART_STOP_BITS;

    BufferedSerial::Flow _UART_HWFLOWCTRL;

    BufferedSerial *serial;

public:
    MbedUartDriver();
    ~MbedUartDriver();

    uint32_t setPin(uint32_t txd, uint32_t rxd);
    uint32_t setConfig(uint32_t baudRate, uint8_t frameSize, uint8_t parityBit, uint8_t stopBit);

    uint32_t setSwFlowControl();
    uint32_t setHwFlowControl(uint32_t rts, uint32_t cts);

    uint32_t init();
    uint32_t deinit();

    uint32_t putChar(char c);
    uint32_t putLine(const char *str);
    char getChar();
    uint32_t getLine(char *str, uint32_t len);
    uint32_t dataAvailable();
    uint32_t powerUp(uint8_t state);
};

#endif