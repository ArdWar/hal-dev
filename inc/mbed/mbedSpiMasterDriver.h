#ifndef _MBEDSPIMASTERDRIVER_H_
#define _MBEDSPIMASTERDRIVER_H_

#include "spiMasterDriver.h"
#include "mbed.h"

class MbedSpiMasterDriver : public SpiMasterDriver
{
private:
    PinName _SPI_MOSI;
    PinName _SPI_MISO;
    PinName _SPI_SCLK;
    uint32_t _SPI_FREQ;
    uint8_t _SPI_MODE;

    SPI *_pSPI;
    DigitalOut *_SPI_CSEL[255];

public:
    MbedSpiMasterDriver();
    ~MbedSpiMasterDriver();

    void setPin(uint32_t mosi, uint32_t miso, uint32_t sclk);
    void setMode(uint8_t mode);
    void setFrequency(uint32_t freq);

    void initCsPin(uint32_t ssel);

    int32_t init();
    void deinit();

    int32_t write(uint8_t *txData, int32_t txLen);
    int32_t read(uint8_t *rxData, int32_t rxLen);
    int32_t writeread(uint8_t *txData, int32_t txLen, uint8_t *rxData, int32_t rxLen);

    void activateDevice(uint32_t ssel);
    void deactivateDevice(uint32_t ssel);
};

#endif