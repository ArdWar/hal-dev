#ifndef _MBEDLOCK_H_
#define _MBEDLOCK_H_

#include "mbed.h"
#include "lockRtos.h"

namespace satunol
{
    class MbedLock : public Lock
    {
    public:
        MbedLock();
        ~MbedLock();

        void lock();
        void unlock();

    private:
        ::Mutex *_pMutex;
    };
} // namespace satunol
#endif