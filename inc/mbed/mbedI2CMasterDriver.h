#ifndef _MBEDI2CMASTERDRIVER_H_
#define _MBEDI2CMASTERDRIVER_H_

#include "i2cMasterDriver.h"
#include "mbed.h"

class MbedI2CMasterDriver : public I2CMasterDriver
{
private:
    PinName _I2C_SDA;
    PinName _I2C_SCL;
    uint32_t _I2C_CLK;
    I2C *_pI2C;

public:
    MbedI2CMasterDriver();
    ~MbedI2CMasterDriver();

    void setPin(uint32_t sda, uint32_t scl);
    void setFrequency(uint32_t clk);

    int32_t init();
    void deinit();

    void active(bool isActive);

    int32_t write(uint8_t addr7, uint8_t *txData, int32_t txLen);
    int32_t read(uint8_t addr7, uint8_t *rxData, int32_t rxLen);

    int32_t writeread(uint8_t addr7, uint8_t *txData, int32_t txLen, uint8_t *rxData, int32_t rxLen);
    int32_t readwrite(uint8_t addr7, uint8_t *rxData, int32_t rxLen, uint8_t *txData, int32_t txLen);
};

#endif