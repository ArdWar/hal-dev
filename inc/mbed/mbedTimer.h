#ifndef _MBEDTIMER_H_
#define _MBEDTIMER_H_

#include "mbed.h"
#include "timerRtos.h"

namespace satunol
{
    class MbedTimer : public Timer
    {
    public:
        MbedTimer();
        ~MbedTimer();

        void setPeriod(uint32_t msPeriod);

        void setHandler(TimerHandler *pHandlerObj);
        void setHandler(void (*pHandlerFunc)());

        void start();
        void stop();

    private:
        ::Ticker *_pMbedTicker;
        std::chrono::microseconds _chronoPeriod;
        ::Callback<void()> _pMbedCallback;
    };
} // namespace satunol

#endif