#ifndef _MBEDTHREAD_H_
#define _MBEDTHREAD_H_

#include "mbed.h"
#include "threadRtos.h"

namespace satunol
{
    class MbedThread : public Thread
    {
    public:
        MbedThread();
        ~MbedThread();

        static Thread *getCurrentThread();

        void setPriority(uint8_t osPriority);
        void setStackSize(uint32_t stackSize);

        void start();
        void stop();
        void sleep(uint32_t msPeriod);

        void setHandler(ThreadHandler *pHandlerObj);
        void setHandler(void (*pHandlerFunc)());

    private:
        Callback<void()> _pMbedCallback;
        ::Thread *_pThread;
    };
} // namespace satunol

#endif