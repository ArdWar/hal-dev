// PA ramping = true 
// Whitening = false 
// Bit rate = 9.6 
// Address config = No address check 
// Packet bit length = 0 
// Packet length mode = Variable 
// Performance mode = High Performance 
// Carrier frequency = 434.000000 
// Deviation = 3.997803 
// Symbol rate = 9.6 
// Manchester enable = false 
// RX filter BW = 25.000000 
// TX power = 15 
// Packet length = 255 
// Modulation format = 2-FSK 
// Device address = 0 

#ifndef CC112X_REG_CONFIG_H
#define CC112X_REG_CONFIG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "cc1120_regaddr.h"

static const registerSetting_t preferredSettings[]= 
{
  {CC112X_IOCFG3,            0xB0},
  {CC112X_IOCFG2,            0x06},
  {CC112X_IOCFG1,            0xB0},
  {CC112X_IOCFG0,            0x40},
  {CC112X_SYNC_CFG1,         0x0B},
  {CC112X_DCFILT_CFG,        0x1C},
  {CC112X_PREAMBLE_CFG1,     0x18},
  {CC112X_IQIC,              0xC6},
  {CC112X_CHAN_BW,           0x08},
  {CC112X_MDMCFG0,           0x05},
  {CC112X_SYMBOL_RATE2,      0x73},
  {CC112X_AGC_REF,           0x20},
  {CC112X_AGC_CS_THR,        0x0C},
  {CC112X_AGC_CFG1,          0xA0},
  {CC112X_FIFO_CFG,          0x00},
  {CC112X_SETTLING_CFG,      0x03},
  {CC112X_FS_CFG,            0x14},
  {CC112X_WOR_CFG0,          0x20},
  {CC112X_WOR_EVENT0_LSB,    0xA1},
  {CC112X_PKT_CFG0,          0x20},
  {CC112X_RFEND_CFG0,        0x0C},
  {CC112X_PA_CFG0,           0x7D},
  {CC112X_PKT_LEN,           0xFF},
  {CC112X_IF_MIX_CFG,        0x00},
  {CC112X_FREQOFF_CFG,       0x22},
  {CC112X_FREQ2,             0x6C},
  {CC112X_FREQ1,             0x80},
  {CC112X_FS_DIG1,           0x00},
  {CC112X_FS_DIG0,           0x5F},
  {CC112X_FS_CAL1,           0x40},
  {CC112X_FS_CAL0,           0x0E},
  {CC112X_FS_DIVTWO,         0x03},
  {CC112X_FS_DSM0,           0x33},
  {CC112X_FS_DVC0,           0x17},
  {CC112X_FS_PFD,            0x50},
  {CC112X_FS_PRE,            0x6E},
  {CC112X_FS_REG_DIV_CML,    0x14},
  {CC112X_FS_SPARE,          0xAC},
  {CC112X_FS_VCO0,           0xB4},
  {CC112X_XOSC5,             0x0E},
  {CC112X_XOSC2,             0x00},
  {CC112X_XOSC1,             0x03},
};

#ifdef __cplusplus
}
#endif

#endif // CC112X_REG_CONFIG_H