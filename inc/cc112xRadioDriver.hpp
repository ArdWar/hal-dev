#ifndef CC112XRADIODRIVER_HPP
#define CC112XRADIODRIVER_HPP

typedef enum _regaccess_
{
    StdRegSpace, // Standart Register Space
    ExtAddr,     // Extended Register Space
    CmdStrobe,   // Command Strobe
    DirectFIFO,  // Direct FIFO Access
    StdFIFO      // Standart FIFO access
} RegAcces;

class Cc112xRadioDriver
{
private:
    // Variable
    unsigned char MOSI_PIN; // member variable
    unsigned char MISO_PIN;
    unsigned char SCLK_PIN;
    unsigned char SSEL_PIN;
    unsigned char SPI_PORT;

    // Addtional Function Used by Core Function
    virtual unsigned char readRxFirst() = 0;
    virtual unsigned char readRxLast() = 0;
    virtual void configSyncWord() = 0;
    // Addtional Function Variable
    unsigned int syncWord;

    // Write & Read Function
    virtual void writeRegSingle(RegAcces access, unsigned char Value, unsigned short Addr) = 0;
    virtual void writeRegBurst(RegAcces access, unsigned char *pData, unsigned char length, unsigned short Addr) = 0;
    virtual unsigned char readRegSingle(RegAcces access, unsigned short Addr) = 0;
    virtual void readRegBurst(unsigned char *pData, unsigned char rxFirst, unsigned char rxLast) = 0; // for now Just for Standart FIFO Access

public:
    // Core Function
    virtual void initCc112x(unsigned char mosi, unsigned char miso, unsigned char clk, unsigned char cs) = 0;
    virtual void deinitCc112x() = 0;
    virtual void cc112xRegisterSet() = 0;
    virtual unsigned char cc112xRegisterCheck() = 0;
    virtual void manualCalibaration() = 0;
    virtual unsigned char readStatus() = 0; // Read Current MARC State
    virtual void flushRxFifo() = 0;         // SFRX Command Strobe
    virtual void flushTxFifo() = 0;         // SFTX Command Strobe
    virtual void send(unsigned char *pData, unsigned int len) = 0;
    virtual unsigned char recv(unsigned char *pData, unsigned char *pRssi, unsigned char *pLen, unsigned short *wakeUpIntv) = 0;
    virtual void readPacket(unsigned char *pData, unsigned char *pLen, unsigned char *pRssi, unsigned char *pLQI, bool *pCrc) = 0;
    virtual void enterSleepMode() = 0;
    virtual void quitSleepMode() = 0;

    // Set Up Function
    virtual void setSniffMode(float sleepTimeout) = 0; // Parameter Notset Yet
    virtual unsigned char setCarrierFreq(unsigned char freq) = 0;
    virtual void setChipAddress(unsigned char address) = 0;
    virtual void setSyncWord(unsigned int value) = 0; //SYNC WORD Value (32-bit) default 0x930B51DE
    virtual void setTxPower(unsigned char txPower) = 0;

    // Debug Function
    virtual void debugModeRegisterCheck() = 0;
};

#endif // CC112XRADIODRIVER_HPP