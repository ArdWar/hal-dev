#ifndef _LPC8XXSPIMASTERDRIVER_H_
#define _LPC8XXSPIMASTERDRIVER_H_

#include "spiMasterDriver.h"
#include "LPC8xx.h"

// SYSAHBCLKCTRL
#define SPI0_CLK (1 << 11)
#define SPI1_CLK (1 << 12)
#define SWM_CLK (1 << 7)

// PRESETCTRL
#define SPI0_RST (1)
#define SPI1_RST (1 << 1)

// SPIx->CFG
#define SPI_CFG_ENABLE (1 << 0)
#define SPI_CFG_MASTER (1 << 2)
#define SPI_CFG_LSBF (1 << 3)
#define SPI_CFG_CPHA (1 << 4)
#define SPI_CFG_CPOL (1 << 5)
#define SPI_CFG_LOOP (1 << 7)
#define SPI_CFG_SPOL (1 << 8)

// SPIx->TXCTL
#define SPI_TX_SSELN (1 << 16)
#define SPI_TX_EOT (1 << 20)
#define SPI_TX_EOF (1 << 21)
#define SPI_TX_RXIGNORE (1 << 22)
#define SPI_TX_LEN8BIT (0x7 << 24)

// SPIx->STAT
#define SPI_STAT_RXRDY (1 << 0)
#define SPI_STAT_TXRDY (1 << 1)

class Lpc8xxSpiMasterDriver : public SpiMasterDriver
{
private:
    uint8_t _SPI_MOSI;
    uint8_t _SPI_MISO;
    uint8_t _SPI_SCLK;

    uint8_t _SPI_MODE;
    uint32_t _SPI_FREQ;

public:
    Lpc8xxSpiMasterDriver();
    ~Lpc8xxSpiMasterDriver();

    void setPin(uint32_t mosi, uint32_t miso, uint32_t sclk);
    void setMode(uint8_t mode);
    void setFrequency(uint32_t freq);

    void initCsPin(uint32_t ssel);

    int32_t init();
    void deinit();

    int32_t write(uint8_t *txData, int32_t txLen);
    int32_t read(uint8_t *rxData, int32_t rxLen);
    int32_t writeread(uint8_t *txData, int32_t txLen, uint8_t *rxData, int32_t rxLen);

    void activateDevice(uint32_t ssel);
    void deactivateDevice(uint32_t ssel);
};

#endif