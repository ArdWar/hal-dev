/**
 * @file    lpc8xxI2CMasterDriver.h
 * @author  www.satunol.com
 * @brief   I2C driver for lpc8xx devices
 *          Dependency : LPC8xx.h from SDK_2.7.0_LPC8xx (you can also use LPC812.h from
 *                       SDK_2.7.0_LPC812 or equivalent file from other member of the family,
 *                       be sure to modify the include path if you do so)
 * @version 0.5
 * @date    2020-07-16
 * 
 * @copyright Copyright (c) 2020
 */

#ifndef _LPC8XXI2CMASTERDRIVER_H_
#define _LPC8XXI2CMASTERDRIVER_H_

#include "LPC8xx.h"
#include "i2cMasterDriver.h"

#define MAXDATALENGTH 255

// ROM_API address for I2C
#define LPC_I2CD_API ((I2CD_API_T *)(*(uint8_t **)((*(uint8_t **)0x1FFF1FF8UL) + 0x14)))

// SYSAHBCLKCTRL
#define I2C_CLK (1 << 5)
#define SWM_CLK (1 << 7)

// PRESETCTRL
#define I2C_RST (1 << 6)

typedef void I2C_HANDLE_T;

typedef void (*I2C_CALLBK_T)(uint32_t err_code, uint32_t n);

typedef struct I2C_PARAM
{
    uint32_t num_bytes_send;
    uint32_t num_bytes_rec;
    uint8_t *buffer_ptr_send;
    uint8_t *buffer_ptr_rec;
    I2C_CALLBK_T func_pt;
    uint8_t stop_flag;
    uint8_t dummy[3];
} I2C_PARAM_T;

typedef struct I2C_RESULT
{
    uint32_t n_bytes_sent;
    uint32_t n_bytes_recd;
} I2C_RESULT_T;

typedef enum I2C_MODE
{
    IDLE,
    MASTER_SEND,
    MASTER_RECEIVE,
    SLAVE_SEND,
    SLAVE_RECEIVE
} I2C_MODE_T;

typedef enum
{
    /*0x00000000*/ LPC_OK = 0,
    /*0x00000001*/ ERROR,
    /*0x00060000*/ ERR_I2C_BASE = 0x00060000,
    /*0x00060001*/ ERR_I2C_NAK,
    /*0x00060002*/ ERR_I2C_BUFFER_OVERFLOW,
    /*0x00060003*/ ERR_I2C_BYTE_COUNT_ERR,
    /*0x00060004*/ ERR_I2C_LOSS_OF_ARBRITRATION,
    /*0x00060005*/ ERR_I2C_SLAVE_NOT_ADDRESSED,
    /*0x00060006*/ ERR_I2C_LOSS_OF_ARBRITRATION_NAK_BIT,
    /*0x00060007*/ ERR_I2C_GENERAL_FAILURE,
    /*0x00060008*/ ERR_I2C_REGS_SET_TO_DEFAULT
} ErrorCode_t;

typedef struct I2CD_API
{
    void (*i2c_isr_handler)(I2C_HANDLE_T *handle);
    uint32_t (*i2c_master_transmit_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
    uint32_t (*i2c_master_receive_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
    uint32_t (*i2c_master_tx_rx_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
    uint32_t (*i2c_master_transmit_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
    uint32_t (*i2c_master_receive_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
    uint32_t (*i2c_master_tx_rx_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
    uint32_t slave_api[5];
    uint32_t (*i2c_get_mem_size)(void);
    I2C_HANDLE_T *(*i2c_setup)(uint32_t i2c_base_addr, uint32_t *start_of_ram);
    uint32_t (*i2c_set_bitrate)(I2C_HANDLE_T *handle, uint32_t p_clk_in_hz, uint32_t bitrate_in_bps);
    uint32_t (*i2c_get_firmware_version)(void);
    I2C_MODE_T (*i2c_get_status)(I2C_HANDLE_T *handle);
    uint32_t (*i2c_set_timeout)(I2C_HANDLE_T *handle, uint32_t timeout);
} I2CD_API_T;

class Lpc8xxI2CMasterDriver : public I2CMasterDriver
{
private:
    uint8_t _I2C_SDA;
    uint8_t _I2C_SCL;
    uint32_t _I2C_CLK;
    I2C_HANDLE_T *i2cHandle;
    uint32_t i2cHandleMEM[0x20];

public:
    Lpc8xxI2CMasterDriver();
    ~Lpc8xxI2CMasterDriver();

    void setPin(uint32_t sda, uint32_t scl);
    void setFrequency(uint32_t clk);

    void active(bool isActive);

    int32_t init();
    void deinit();

    int32_t write(uint8_t addr7, uint8_t *txData, int32_t txLen);
    int32_t read(uint8_t addr7, uint8_t *rxData, int32_t rxLen);
    int32_t writeread(uint8_t addr7, uint8_t *txdata, int32_t txlen, uint8_t *rxdata, int32_t rxlen);
    int32_t readwrite(uint8_t addr7, uint8_t *rxdata, int32_t rxlen, uint8_t *txdata, int32_t txlen);
};

#endif