#ifndef _LPC8XXUARTDRIVER_H_
#define _LPC8XXUARTDRIVER_H_

#include <stdint.h>
#include <cstring>
#include "LPC8xx.h"
#include "uartDriver.h"
#include "SEGGER_RTT.h"

#define LPC_UARTD_API ((UARTD_API_T *)(*(uint8_t **)((*(uint8_t **)0x1FFF1FF8UL) + 0x24)))

// SYSAHBCLKCTRL
#define SWM_CLK (1 << 7)
#define UART0_CLK (1 << 14)
#define UART1_CLK (1 << 15)
#define SWM_CLK (1 << 7)

// PRESETCTRL
#define UARTFRG_RST_N (1 << 2)
#define UART0_RST_N (1 << 3)
#define UART1_RST_N (1 << 4)

// USARTX_STAT
#define STAT_RXRDY (1 << 0)
#define STAT_RXIDLE (1 << 1)
#define STAT_TXRDY (1 << 2)
#define STAT_TXIDLE (1 << 3)
#define STAT_CTS (1 << 4)
#define STAT_DELTACTS (1 << 5)
#define STAT_OVERRUNINT (1 << 7)
#define STAT_RXBRK (1 << 10)
#define STAT_DELTARXBRK (1 << 11)
#define STAT_START (1 << 12)
#define STAT_FRAMERRINT (1 << 13)
#define STAT_PARITYERRINT (1 << 14)
#define STAT_RXNOISEINT (1 << 15)

typedef void UART_HANDLE_T;

typedef void (*UART_CALLBK_T)(uint32_t err_code, uint32_t n);

typedef struct UART_PARAM
{
    const char *buffer;
    uint32_t size;
    uint16_t trm_mode;
    uint16_t drv_mode;
    UART_CALLBK_T func_pt;
} UART_PARAM_T;

typedef struct UART_CONFIG
{
    uint32_t sys_clk_in_hz;
    uint32_t baudrate_in_hz;
    uint8_t config;
    uint8_t sync_mod;
    uint16_t error_en;
} UART_CONFIG_T;

typedef enum UART_TRM_MODE
{
    NOTERM,   // get_line() and put_line() ignore termination
    CRLFTERM, // get_line() terminates on \r\n; put_line replace \0 with \r\n then terminates
    LFTERM,   // get_line() terminates on \n; put_line replace \0 with \n then terminates
    NULLTERM, // put_line() terminates on \0
} UART_TRM_MODE_T;

typedef enum UART_DRV_MODE
{
    POLL,
    INTR,
} UART_DRV_MODE_T;

typedef struct UARTD_API
{
    uint32_t (*uart_get_mem_size)(void);
    UART_HANDLE_T *(*uart_setup)(uint32_t base_addr, uint8_t *ram);
    uint32_t (*uart_init)(UART_HANDLE_T *handle, UART_CONFIG_T *set);

    uint8_t (*uart_get_char)(UART_HANDLE_T *handle);
    void (*uart_put_char)(UART_HANDLE_T *handle, uint8_t data);
    uint32_t (*uart_get_line)(UART_HANDLE_T *handle, UART_PARAM_T *param);
    uint32_t (*uart_put_line)(UART_HANDLE_T *handle, UART_PARAM_T *param);

    void (*uart_isr)(UART_HANDLE_T *handle);
} UARTD_API_T;

class Lpc8xxUartDriver : public UartDriver
{
private:
    uint8_t _UART_TXD;
    uint8_t _UART_RXD;
    uint8_t _UART_CTS;
    uint8_t _UART_RTS;

    UART_CONFIG_T _UART_CONFIG;

    UART_HANDLE_T *uartHandle;
    uint8_t uartHandleMEM[40];

public:
    Lpc8xxUartDriver();
    ~Lpc8xxUartDriver();

    uint32_t setPin(uint32_t txd, uint32_t rxd);
    uint32_t setConfig(uint32_t baudRate, uint8_t frameSize, uint8_t parityBit, uint8_t stopBit);
    uint32_t setSwFlowControl();
    uint32_t setHwFlowControl(uint32_t rts, uint32_t cts);

    uint32_t init();
    uint32_t deinit();

    uint32_t putChar(char c);
    uint32_t putLine(const char *str);
    char getChar();
    uint32_t getLine(char *str, uint32_t len);
    uint32_t dataAvailable();
    uint32_t powerUp(uint8_t state);
};

#endif