#ifndef CC1120RADIODRIVER_HPP
#define CC1120RADIODRIVER_HPP

#include "lpc8xxSpiMasterDriver.h"
#include "cc112xRadioDriver.hpp"

extern "C"
{
#include "cc1120_regaddr.h"
#include "cc1120_register_config.h"
#include "SEGGER_RTT.h"
#include "LPC8xx.h"
}

#define SYSTEM_CORE_CLOCK 12000000UL
// Manual Calibration Macro
#define VCDAC_START_OFFSET 2
#define FS_VCO2_INDEX 0
#define FS_VCO4_INDEX 1
#define FS_CHP_INDEX 2
// Set Freq macro
#define CC112X_FREQ_BASE 0x00668000l // frequency base is 410Mhz, max 480Mhz
#define CC112X_FREQ_STEP 0x4000      // frequency channel stap is 1Mhz

class Cc1120RadioDriver : public Cc112xRadioDriver
{
private:
    Lpc8xxSpiMasterDriver spi;

    // Delay

    // Variable
    unsigned char MOSI_PIN; // member variable
    unsigned char MISO_PIN;
    unsigned char SCLK_PIN;
    unsigned char SSEL_PIN;
    unsigned char SPI_PORT;

    // Addtional Function Used by Core Function
    virtual unsigned char readRxFirst();
    virtual unsigned char readRxLast();
    virtual void configSyncWord();
    // Addtional Function Variable

    // Write & Read Function
    virtual void writeRegSingle(RegAcces access, unsigned char Value, unsigned short Addr);
    virtual void writeRegBurst(RegAcces access, unsigned char *pData, unsigned char length, unsigned short Addr);
    virtual unsigned char readRegSingle(RegAcces access, unsigned short Addr);
    virtual void readRegBurst(unsigned char *pData, unsigned char rxFirst, unsigned char rxLast); // for now Just for Standart FIFO Access

public:
    // Constructor & Destructor
    Cc1120RadioDriver();
    virtual ~Cc1120RadioDriver();

    // Core Function
    virtual void initCc112x(unsigned char mosi, unsigned char miso, unsigned char clk, unsigned char cs);
    virtual void deinitCc112x();
    virtual void cc112xRegisterSet();
    virtual unsigned char cc112xRegisterCheck();
    virtual void manualCalibaration();
    virtual unsigned char readStatus(); // Read Current MARC State
    virtual void flushRxFifo();         // SFRX Command Strobe
    virtual void flushTxFifo();         // SFTX Command Strobe
    virtual void send(unsigned char *pData, unsigned int len);
    virtual unsigned char recv(unsigned char *pData, unsigned char *pRssi, unsigned char *pLen, unsigned short *wakeUpIntv);
    virtual void readPacket(unsigned char *pData, unsigned char *pLen, unsigned char *pRssi, unsigned char *pLQI, bool *pCrc);
    virtual void enterSleepMode();
    virtual void quitSleepMode();

    // Set Up Function
    virtual void setSniffMode(float sleepTimeout); // Parameter Not set Yet
    virtual unsigned char setCarrierFreq(unsigned char freq);
    virtual void setChipAddress(unsigned char address);
    virtual void setSyncWord(unsigned int value); //SYNC WORD Value (32-bit) default 0x930B51DE
    virtual void setTxPower(unsigned char txPower);

    // Debug Function
    virtual void debugModeRegisterCheck();
};

#endif // CC1120RADIODRIVER_HPP