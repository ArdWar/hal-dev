# set the name of the operating system for which CMake is to build
set(CMAKE_SYSTEM_NAME Generic) # ini nentuin dicompile untuk OS apa, selain Generic, ada Linux 
set(CMAKE_CROSSCOMPILING TRUE) # ini nentuin cross compile apa bukan
# specify the cross compiler
set(CMAKE_ASM_COMPILER  /opt/gcc-arm-none-eabi-8-2019-q3-update/bin/arm-none-eabi-gcc)
set(CMAKE_C_COMPILER    /opt/gcc-arm-none-eabi-8-2019-q3-update/bin/arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER  /opt/gcc-arm-none-eabi-8-2019-q3-update/bin/arm-none-eabi-g++)
set(ELF2BIN             /opt/gcc-arm-none-eabi-8-2019-q3-update/bin/arm-none-eabi-objcopy)
set(SIZEPROG            /opt/gcc-arm-none-eabi-8-2019-q3-update/bin/arm-none-eabi-size)
set(CROSS_COMPILER_PREFIX arm-none-eabi-) # kalo mau manggil ${CROSS_COMPILER_PREFIX}gcc (coba pake prefix)
# cross compiler flags
set(CMAKE_ASM_FLAGS " -x assembler-with-cpp -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -Werror -fmessage-length=0 -fno-exceptions -funsigned-char -fno-delete-null-pointer-checks -fomit-frame-pointer -Og -g3 -mcpu=cortex-m0plus -mthumb -mno-apcs-frame")
set(CMAKE_C_FLAGS "-Os -fomit-frame-pointer -fsingle-precision-constant -Wdouble-promotion -ffunction-sections -fdata-sections -mcpu=cortex-m0plus -mthumb -mno-apcs-frame -Wl,--unresolved-symbols=ignore-in-object-files")
set(CMAKE_CXX_FLAGS " -std=gnu++11 -fno-rtti --specs=nano.specs -Wvla -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -fmessage-length=0 -fno-exceptions -ffunction-sections -fdata-sections -funsigned-char -fno-delete-null-pointer-checks -fomit-frame-pointer -Og -g3 -mcpu=cortex-m0plus -mthumb")
set(CMAKE_CXX_LINK_FLAGS "-Wl,--gc-sections -nostartfiles -Wl,--script=/home/ardwar/projects/lpc812-rom-libdev/LPC812.ld -Wl,--print-memory-usage")
# cross compiler settings
set(CMAKE_C_COMPILER_WORKS TRUE) # meng-ignore hasil compile-tes, agar tidak error ketika cmake membuat make file 
set(CMAKE_CXX_COMPILER_WORKS TRUE) # meng-ignore hasil compile-tes, agar tidak error ketika cmake membuat make file 
