# LPC812 Driver Library Development

### Prerequisites

This program is tested under following environment :

* CMake version 3.17.0
* GCC compiler version - "gcc-arm-none-eabi-8-2019-q3-update"
    * will *NOT* work on "gcc-arm-none-eabi-9"
* Jlink Edu version 10.1
* Modified TH SENSOR-RF Board with SWCLK and SWDIO connected to Jlink instead of cc1120
* Visual Studio Code version 1.46
* Linux Mint 19.3 Cinnamon

## Getting Started

Take these steps if you want to build it :

* Clone this repo 
* Change the path in "small-arm.cmake" file
* Take these steps to build :
    * Open the cloned repo

    ```
    cd th-controller
    ```
    * Then create build folder and open it

    ```
    mkdir build && cd build
    ```
    * Execute cmake command with toolchain file
    
    ```
    rm -r ./* && cmake -DCMAKE_TOOLCHAIN_FILE=small-arm.cmake .. && make
    ```
* Fuila, then you can connect the board with jlink 
* Don't forget to open JFlashLiteExe and JLinkRTTViewerExe with sudo command
* Upload the program file that formatted with *.hex in build folder with JFlashLiteExe
* You can monitor the controller status and state with JLinkRTTViewerExe

## Deployment

This is not a final form of project, so there will be another revision of files within

## Built With

* [Visual Studio Code](https://code.visualstudio.com/) - Open Source Code Editor
* [GCC Arm Compiler](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) - GCC Compiler
* [Cmake](https://cmake.org/download/) - Used to build things up
* [ModbusIno Lib](https://github.com/stephane/modbusino) - for Modbus Protocol
* [LPCOpen](https://www.nxp.com/downloads/en/libraries/lpcopen_3_02_lpcxpresso_nxp_lpcxpresso_812.zip) - LPC812 Code Application
* [SWRC253E](http://www.ti.com/lit/zip/swrc253) - CC112x Code Example (SWRC253E)

## Authors

* **Pak Arianto** - *Supervisor*
* **Pak Syamsul** - *Initial work*
* **Ilham** - *Developing further* 
* **Benita** - *Developing further*
* **All Satunol Developer Team that makes this project works**

## License

This project is licensed under the MIT License
